OpenMap

An application for POI management

Features:
- create a new collection of POIs - map (with the definition of POI's attributes)
- create a new POI on the map
- show map
- filter POIs by attributes


export GOOGLE_APPLICATION_CREDENTIALS="/home/mano/dev/mano-openmap/mano-openmap-c349d93bf511.json"
