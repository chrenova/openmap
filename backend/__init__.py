from flask import Flask, redirect, url_for, render_template
from flask.json import JSONEncoder
from google.cloud.datastore.helpers import GeoPoint


class CustomJSONEncoder(JSONEncoder):

    def default(self, obj):
        try:
            if isinstance(obj, GeoPoint):
                return {'lat': obj.latitude, 'lon': obj.longitude}
            iterable = iter(obj)
        except TypeError:
            pass
        else:
            return list(iterable)
        return JSONEncoder.default(self, obj)


def create_app(config):
    app = Flask(__name__)
    app.json_encoder = CustomJSONEncoder
    app.config.from_object(config)

    with app.app_context():
        model = get_model(app.config['DATA_BACKEND'])
        model.init_app(app.config['PROJECT_ID'])
        #firebase_model = get_firebase_model()
        #firebase_model.init_app(app)

    from .openfood import openfood
    app.register_blueprint(openfood, url_prefix='/openfood')

    @app.route('/')
    def index():
        return render_template('index.html')

    return app


def get_model(data_backend):
    model_backend = data_backend # current_app.config['DATA_BACKEND']
    if model_backend == 'cloudsql':
        from . import model_cloudsql
        model = model_cloudsql
    elif model_backend == 'datastore':
        from . import model_datastore
        model = model_datastore
    elif model_backend == 'mongodb':
        from . import model_mongodb
        model = model_mongodb
    else:
        raise ValueError(
            "No appropriate databackend configured. "
            "Please specify datastore, cloudsql, or mongodb")

    return model
