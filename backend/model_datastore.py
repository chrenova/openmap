#from flask import current_app
from google.cloud import datastore
from google.cloud.datastore.helpers import GeoPoint

DATASTORE_KIND_MAP = 'Map'
DATASTORE_KIND_POI = 'POI2'
DATASTORE_KIND_USER = 'User'

builtin_list = list
#client = None
project_id = None

def init_app(google_project_id):
    #global client
    #from google.oauth2 import service_account
    #credentials = service_account.Credentials.from_service_account_file(current_app.config['SERVICE_ACCOUNT_DATASTORE'])
    #client = datastore.Client(project=current_app.config['PROJECT_ID'], credentials=credentials)
    #client = datastore.Client(project=current_app.config['PROJECT_ID'])
    global project_id
    project_id = google_project_id

def get_client():
    #from google.oauth2 import service_account
    #print(current_app.config)
    #credentials = service_account.Credentials.from_service_account_file(current_app.config['SERVICE_ACCOUNT_DATASTORE'])
    #return datastore.Client(project=current_app.config['PROJECT_ID'], credentials=credentials)
    return datastore.Client(project=project_id)

def list(collection):
    client = get_client()
    parent_key = client.key(DATASTORE_KIND_MAP, collection) if collection else None
    query = client.query(kind=DATASTORE_KIND_POI, ancestor=parent_key, order=[])
    return builtin_list(map(from_datastore, query.fetch()))

def list_maps(user):
    client = get_client()
    #parent_key = client.key('User', user) if user else None
    #query = client.query(kind='POICollection', ancestor=parent_key, order=[])
    query = client.query(kind=DATASTORE_KIND_MAP, order=[])
    return builtin_list(map(from_datastore, query.fetch()))

def save(properties, lat, lon, collection, id=None):
    client = get_client()
    parent_key = client.key(DATASTORE_KIND_MAP, collection)
    if id:
        k = client.key(DATASTORE_KIND_POI, id, parent=parent_key)
    else:
        k = client.key(DATASTORE_KIND_POI, parent=parent_key)
    e = datastore.Entity(key=k)
    e['latlon'] = GeoPoint(lat, lon)
    e.update(properties)
    client.put(e)


def distinct_types():
    client = get_client()
    query = client.query(kind='OpenFood')
    query.distinct_on = ['type']
    query.order = ['type']
    query.projection = ['type']
    return builtin_list(query.fetch())

def from_datastore(entity):
    if not entity:
        return None
    if isinstance(entity, builtin_list):
        entity = entity.pop()

    entity['id'] = entity.key.id
    # TODO implement setting the latlon here, not in custom json encoder
    return entity

def create_poi_collection(id):
    client = get_client()
    k = client.key('POICollection', id)
    e = datastore.Entity(key=k)
    client.put(e)
    return k

def create_poi_with_ancestor(parent_key, properties):
    client = get_client()
    k = client.key('POI', parent=parent_key)
    e = datastore.Entity(key=k)
    e.update(properties)
    client.put(e)

def list_collections():
    client = get_client()
    query = client.query(kind='POICollection', order=[])
    #return builtin_list(map(from_datastore, query.fetch()))
    return builtin_list(query.fetch())

def list_pois(parent = None, filter_equals = None, filter_greaterequals = None):
    client = get_client()
    ancestor = client.key('POICollection', parent) if parent else None
    query = client.query(kind='POI', ancestor=ancestor, order=[])
    if filter_equals:
        query.add_filter('a', '=', filter_equals)
    if filter_greaterequals:
        query.add_filter('a', '>=', filter_greaterequals)
    #return builtin_list(map(from_datastore, query.fetch()))
    return builtin_list(query.fetch())

def list_pois_by_distance():
    client = get_client()
    query = client.query(kind='POI', order=[])
    query.add_filter('distance(latlon, geopoint(48.15, 17.1))', '<', '30000')
    return builtin_list(query.fetch())

def find_map(name):
    client = get_client()
    query = client.query(kind='Map')
    query.add_filter('name', '=', name)
    return builtin_list(query.fetch())


#============
def create_user(properties):
    client = get_client()
    k = client.key('User')
    e = datastore.Entity(key=k)
    e.update(properties)
    client.put(e)
    return e

def create_map(properties):
    client = get_client()
    k = client.key('Map')
    e = datastore.Entity(key=k)
    e.update(properties)
    client.put(e)
    return e
