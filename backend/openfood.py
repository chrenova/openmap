from backend import get_model
from flask import jsonify, Blueprint, request, current_app

# from firebase_admin import auth

openfood = Blueprint('openfood', __name__)


def as_geojson(poi):
    latlon = poi.pop('latlon')
    properties = dict()
    properties['id'] = poi.key.id_or_name
    properties['collection'] = poi.key.parent.id_or_name
    properties.update(poi)

    geojson = dict()
    geojson['type'] = 'Feature'
    geojson['properties'] = properties
    geojson['geometry'] = {'type': 'Point', 'coordinates': [latlon.longitude, latlon.latitude]}

    #print(geojson)
    return geojson

def serialize_map(map):
    res = dict()
    res['id'] = map.key.id_or_name
    #res['created_by'] = map['created_by'].to_legacy_urlsafe()
    if 'fields' in map:
        res['fields'] = map['fields']
    res['name'] = map['name']
    return res

@openfood.route('/<int:collection>', methods=['GET'])
def list(collection):
    pois = get_model(current_app.config['DATA_BACKEND']).list(collection)
    pois = map(as_geojson, pois)
    return jsonify(pois=pois)

@openfood.route('/maps/<string:user>', methods=['GET'])
def list_maps(user):
    maps = get_model(current_app.config['DATA_BACKEND']).list_maps(user)
    maps = map(serialize_map, maps)
    return jsonify(maps=maps)

@openfood.route('/', methods=['POST'])
def save():
    request_json = request.get_json()
    properties = request_json['properties']
    id = properties.pop('id', None)
    collection = properties.pop('collection')
    lon, lat = request_json['geometry']['coordinates']
    poi = get_model(current_app.config['DATA_BACKEND']).save(properties, lat, lon, collection, id)
    # return jsonify(poi=poi)
    return jsonify({})


@openfood.route('/types', methods=['GET'])
def distinct_types():
    types = get_model(current_app.config['DATA_BACKEND']).distinct_types()
    return jsonify(types=types)
