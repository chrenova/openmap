import logging
import backend
import config


app = backend.create_app(config)

if __name__ == '__main__':
    app.run()
