import pprint
import config
from backend import get_model
from google.cloud.datastore.helpers import GeoPoint

model = get_model(config.DATA_BACKEND)
model.init_app(config.PROJECT_ID)

def create_data():
    for map in ('Map_1', 'Map_2', 'Map_3'):
        parent_key = model.create_poi_collection(map)
        print(parent_key)
        for i in (1, 2, 3):
            properties = {'type': 'gastan', 'latlon': GeoPoint(48+i*0.01, 17+i*0.01)}
            model.create_poi_with_ancestor(parent_key, i, properties)

#create_data()

print('0')
pois = model.list_collections()
pprint.pprint(pois)

print('1')
pois = model.list_pois(parent=None)
for poi in pois:
    print(poi.key.parent.kind, poi.key.parent.id_or_name)
#pprint.pprint(pois)

print('2')
pois = model.list_pois(parent='Map_1')
pprint.pprint(pois)

print('3')
pois = model.list_pois(parent='Map_1', filter_equals=2)
pprint.pprint(pois)

print('4')
pois = model.list_pois(parent=None, filter_equals=2)
pprint.pprint(pois)

print('5')
pois = model.list_pois_by_distance()
pprint.pprint(pois)

'''
google.api_core.exceptions.FailedPrecondition: 400 no matching index found. recommended index is:
- kind: POI
  ancestor: yes
  properties:
  - name: a

'''
print('11')
pois = model.list_pois(parent='Map_1', filter_greaterequals=2)
pprint.pprint(pois)
