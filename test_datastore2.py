import pprint
import config
from backend import get_model
from google.cloud.datastore.helpers import GeoPoint

model = get_model(config.DATA_BACKEND)
model.init_app(config.PROJECT_ID)

pois = model.list('5694955355897856')
pprint.pprint(pois)
