import config
from backend import get_model
from google.cloud import datastore

model = get_model(config.DATA_BACKEND)
model.init_app(config.PROJECT_ID)

user = model.create_user({'name': 'admin'})
map_1 = model.create_map({'name': 'Map_1', 'created_by': user.key})
map_2 = model.create_map({'name': 'Map_2', 'created_by': user.key})
map_3 = model.create_map({'name': 'Map_3', 'created_by': user.key})

client = model.get_client()

pois = model.list_pois(None)
for poi in pois:
    parent_key_id = poi.key.parent.id_or_name
    print(parent_key_id)

    properties = dict(poi.items())

    m = None
    if parent_key_id == 'Map_1':
        m = map_1.key
    if parent_key_id == 'Map_2':
        m = map_2.key
    if parent_key_id == 'Map_3':
        m = map_3.key

    k = client.key('POI2', parent=m)
    e = datastore.Entity(key=k)
    e.update(properties)
    client.put(e)
