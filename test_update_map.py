import config
from backend import get_model
import json

model = get_model(config.DATA_BACKEND)
model.init_app(config.PROJECT_ID)

maps = model.find_map('Map_1')
map = maps[0]

fields = {
    'fields': [
        {
            'name': 'type',
            'type': 'datalist',
            'title': 'Type',
            'section': 1
        },
        {
            'type': 'group',
            'title': 'Availability',
            'section': 2,
            'fields': [
                {
                    'name': 'availability_pos',
                    'type': 'checkbox',
                    'title': '+'
                },
                {
                    'name': 'availability_neg',
                    'type': 'checkbox',
                    'title': '-'
                }
            ]
        },
        {
            'type': 'group',
            'title': 'Quality',
            'section': 2,
            'fields': [
                {
                    'name': 'quality_pos',
                    'type': 'checkbox',
                    'title': '+'
                },
                {
                    'name': 'quality_neg',
                    'type': 'checkbox',
                    'title': '-'
                }
            ]
        },
        {
            'name': 'quantity',
            'type': 'input',
            'title': 'Quantity',
            'section': 2
        },
        {
            'name': 'note',
            'type': 'input',
            'title': 'Note',
            'section': 2
        }
    ]
}

fields_str = json.dumps(fields)
map['fields'] = fields_str
client = model.get_client()
client.put(map)
